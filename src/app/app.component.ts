import { Component, OnInit } from '@angular/core';
import {MapLoaderServiceService} from './game/mapLoader/map-loader-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ankap-matt-proj';

  constructor() {
  }
}
