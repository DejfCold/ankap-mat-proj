import { Component, OnInit } from '@angular/core';
import { MapLoaderServiceService } from 'app/game/mapLoader/map-loader-service.service';
import { GameState } from 'app/game/game-state';
import { Room } from 'app/game/room';
import { GameObject, Action, OnAction } from 'app/game/game-object';

@Component({
  selector: 'app-game-screen',
  templateUrl: './game-screen.component.html',
  styleUrls: ['./game-screen.component.scss']
})
export class GameScreenComponent implements OnInit {
  gameState: GameState;
  gameMap: Room[];

  constructor(private mapLoaderServiceService: MapLoaderServiceService) {
  }

  ngOnInit(): void {
    this.gameMap = this.mapLoaderServiceService.loadMap();
    this.gameState = {
      playerName: 'Test',
      playerHealth: 10,
      playerStamina: 10,
      currentRoom: this.gameMap[0],
      messageLog: [],
      inventory: []
    };
    console.log(this.gameMap);
  }

  exitsToString() {
    const names = this.gameState.currentRoom.exits.map(x => x.name);
    return names;
  }

  goToRoom(room: Room) {
    this.gameState.previousRoom = this.gameState.currentRoom;
    this.gameState.currentRoom = room;
    this.gameState.messageLog = [];
  }

  gameObjectAction(gameObject: GameObject, roomObjectAction: string, parent?: GameObject) {
    console.log(gameObject, roomObjectAction, Action.ADD_TO_INVENTORY);
    const isAction = (x: OnAction) => {
      return x.actionName === roomObjectAction;
    };
    gameObject.onAction.filter(x => x.getActionType() === roomObjectAction)
      .forEach(x => x.doAction(this.gameState, gameObject, parent));

    const actionMessagesForAttached = () => {
      const v = gameObject.attachedGameObjects.map(toOnAction);
      const b = v.map(x => x.map(y => y.text));
      const n = [].concat.apply([], b);
      return n;
    };
    const toOnAction = (child: GameObject) => child.onParentAction.filter(isAction);
    const actionMessages = actionMessagesForAttached();
    actionMessages.forEach(x => this.gameState.messageLog.push(x));
  }

  returnToRoom() {
    this.gameState.examinedObject = undefined;
  }
  isExaminingObject(): boolean {
    return this.gameState.examinedObject !== undefined;
  }
}
