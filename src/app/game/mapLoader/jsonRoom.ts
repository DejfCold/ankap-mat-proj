interface JsonRoom {
    id: number;
    name: string;
    description?: string;
    exits: number[];
    onEnterMessages: string[];
    gameObjects: number[];
}
interface JsonGameObject {
    id: number;
    name: string;
    description?: string;
    attachedGameObjects: number[];
    actions: string[];
    onAction: JsonOnAction[];
    onParentAction?: JsonOnAction[];
}
interface JsonOnAction {
    actionName: string;
    text: string;
    description: string;
}
