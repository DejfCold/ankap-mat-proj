import { Injectable } from '@angular/core';
import gameMapFile from '../../../assets/gameMap.json';
import { LiteralMapEntry } from '@angular/compiler/src/output/output_ast';
import { GameObject, ActionFactory } from '../game-object.js';
import { Room } from '../room.js';

@Injectable({
  providedIn: 'root'
})
export class MapLoaderServiceService {

  constructor() { }
  public loadMap(): Room[] {
    const jsonRooms: JsonRoom[] = gameMapFile.rooms;
    const jsonObjects: JsonGameObject[] = gameMapFile.objects;
    const objects = this.jsonObjectsToObjects(jsonObjects);
    const rooms = this.jsonRoomsToRooms(jsonRooms, objects);
    return rooms;
  }

  private jsonObjectsToObjects(jsonObjects: JsonGameObject[]): Map<string, GameObject> {
    const objects = new Map(jsonObjects.map(this.jsonObjectToObjectEntry));
    jsonObjects.forEach(el => {
      const object = objects.get(String(el.id));
      object.attachedGameObjects = el.attachedGameObjects.map(id => objects.get(String(id)));
    });
    return objects;
  }

  private jsonObjectToObjectEntry(jsonObject: JsonGameObject): [string, GameObject] {
    return [
      String(jsonObject.id),
      {
        name: jsonObject.name,
        actions: jsonObject.actions,
        attachedGameObjects: [],
        onAction: jsonObject.onAction.map(x => ActionFactory.createAction(x)),
        onParentAction: jsonObject.onParentAction
      }];
  }

  private jsonRoomsToRooms(gameMap: JsonRoom[], objects: Map<string, GameObject>): Room[] {
    const rooms = new Map(gameMap.map(this.jsonRoomToRoomEntry));
    gameMap.forEach(el => {
      const room = rooms.get(String(el.id));
      room.exits = el.exits.map(id => rooms.get(String(id)));
      el.gameObjects.forEach(go => room.gameObjects.push(objects.get(String(go))));
    });

    return Array.from(rooms.values());
  }

  private jsonRoomToRoomEntry(jsonRoom: JsonRoom): [string, Room] {
    return [
      String(jsonRoom.id),
      {
        name: jsonRoom.name,
        description: jsonRoom.description,
        onEnterMessages: jsonRoom.onEnterMessages,
        exits: [],
        gameObjects: []
    }];
  }

}

