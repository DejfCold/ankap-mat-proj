import { GameObject } from './game-object';

export interface Room {
    name?: string;
    description?: string;
    exits: Room[];
    onEnterMessages: string[];
    gameObjects: GameObject[];
}
