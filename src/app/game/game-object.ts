import { GameState } from './game-state';

export interface GameObject {
    name: string;
    description?: string;
    attachedGameObjects: GameObject[];
    actions: string[];
    onAction: Action[];
    onParentAction?: OnAction[];
}

export interface OnAction {
    actionName: string;
    text: string;
    description: string;
}

export abstract class Action {
    static readonly EXAMINE = 'examine';
    static readonly ADD_TO_INVENTORY = 'addToInventory';
    static readonly USE = 'use';
    
    actionName: string;
    text: string;
    description: string;

    constructor(onAction: OnAction){
        this.actionName = onAction.actionName;
        this.text = onAction.text;
        this.description = onAction.description;
    }
    public abstract doAction(state: GameState, self: GameObject, parent: GameObject);
    public abstract getActionType(): string;
}
export class ActionFactory {
    public static createAction(onAction: OnAction): Action {
        if (onAction.actionName === Action.EXAMINE) {
            return new ExamineAction(onAction);
        }
        if (onAction.actionName === Action.ADD_TO_INVENTORY) {
            return new AddToInventoryAction(onAction);
        }
        if (onAction.actionName === Action.USE) {
            return new UseAction(onAction);
        }
        console.log('WRONG ACTION!');
        return null;
    }
}

export class ExamineAction extends Action {
    constructor(onAction: OnAction) {
        super(onAction);
    }
    public doAction(state: GameState, self: GameObject, parent: GameObject) {
        state.messageLog.push(this.text);
        state.examinedObject = self;
    }
    public getActionType() {
        return Action.EXAMINE;
    }
}
export class AddToInventoryAction extends Action {
    constructor(onAction: OnAction) {
        super(onAction);
    }
    public doAction(state: GameState, self: GameObject, parent: GameObject) {
        state.messageLog.push(this.text);
        state.inventory.push(self);
        parent.attachedGameObjects = parent.attachedGameObjects.filter(x => x !== self);
    }
    public getActionType() {
        return Action.ADD_TO_INVENTORY;
    }
}
export class UseAction extends Action {
    constructor(onAction: OnAction) {
        super(onAction);
    }
    public doAction(state: GameState, self: GameObject, parent: GameObject) {
        state.messageLog.push(this.text);
    }
    public getActionType() {
        return Action.USE;
    }
}
