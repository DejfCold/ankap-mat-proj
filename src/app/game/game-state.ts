import { GameObject } from './game-object';
import { Room } from './room';

export interface GameState {
    playerName: string;
    playerHealth: number;
    playerStamina: number;
    currentRoom?: Room;
    previousRoom?: Room;
    messageLog: string[];
    inventory: GameObject[];
    examinedObject?: GameObject;
}
