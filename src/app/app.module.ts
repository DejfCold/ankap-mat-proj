import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { GameScreenComponent } from './game-screen/game-screen.component';
import { AppRoutingModuleModule } from './app-routing-module.module';
import { SettingsComponent } from './settings/settings.component';
import { LoadGameComponent } from './load-game/load-game.component';
import { AboutComponent } from './about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    MainMenuComponent,
    GameScreenComponent,
    SettingsComponent,
    LoadGameComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModuleModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
