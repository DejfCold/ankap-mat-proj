import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { GameScreenComponent } from './game-screen/game-screen.component';
import { SettingsComponent } from './settings/settings.component';
import { LoadGameComponent } from './load-game/load-game.component';

const routes: Routes = [
  { path: 'menu', component: MainMenuComponent },
  { path: 'game', component: GameScreenComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'load', component: LoadGameComponent },
  { path: 'about', component: LoadGameComponent },
  { path: '', redirectTo: '/menu', pathMatch: 'full' },
  { path: '**', redirectTo: '/menu'}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModuleModule { }
